library(shiny)
library(RPostgreSQL)
library(rworldmap)
library(ggplot2)

#Povezava z  bazo
source("./uvoz_v_bazo/auth-public.R")
conn <- src_postgres(user=user , password=password, dbname=db, host=host)

#Globalne spremenljivke
t<-data.frame()
vrstica<-data.frame()

server.R<-shinyServer(function(input, output) {

  
  tbl.disaster <- tbl(conn, "disaster")
  tbl.disaster_type <- tbl(conn,"disaster_type")
  tbl.country <- tbl(conn,"country")
  
  skupnaTabela1 <- inner_join(tbl.disaster,tbl.disaster_type, by = "disaster_type_id")
  skupnaTabela2 <- inner_join(skupnaTabela1,tbl.country, by = "country_id")
  

    #Priprava tabele
    tabela <- renderTable({
    t <- data.frame(arrange(filter(skupnaTabela2, year==input$obmocje),deaths))
    
    tipi_nesrec <- data.frame(tbl.disaster_type)
    tipi_nesrec <- tipi_nesrec[,2]
    
    prevodi <- c("Epidemija", "Suša", "Vulkanska aktivnost", "Poplava", "Nevihta", "Potres",
                 "Nesreča v industriji", "Razno", "Plaz", "Transportna nesreča", "Plaz",
                 "Požar","Napad žuželk", "Kompleksne nesreče", "Ekstremne temperature", "Napad živali")
    slovar <- data.frame("english" = tipi_nesrec,"slovenian" = prevodi )
    povezava <- match(t[,11],slovar$english)
    povezava2 <- prevodi[povezava]
    
    t <- data.frame(t[,14], povezava2, t[,4], t[,5], t[,6], t[,7], t[,8], t[,9], t[,10],t[,19])
    colnames(t) <- c("Država", "Tip nesreče", "Leto", "Pogostost", "Število smrti","Število ranjenih",
                     "Oškodovani", "Brez domov", "Škoda", "Pripravljenost države")
    
    if (input$izberi_tip != "Vse"){
      t <- t[povezava2 == input$izberi_tip,]
    }
    
    t<<-t[order(t[input$izbor],decreasing=input$uredi),]# order uredi stolpec Skoda v padajoÄŤem vrstnem redu
    
  })
  

  
 
  #Izpiše tabelo 
  output$disaster<-tabela
  
  
  #Izvozi tabelo

  output$downloadData <- downloadHandler(
    filename <- function() {
     'Naravne_nesrece.csv'
    },
    content <- function(file) {
      write.csv(t, file)
    }
  )
  
  
  #Izpiše histogram
  output$stevilonesrec<- renderPlot({
    
    t <- data.frame(tbl.disaster)
    a <- t[,"year"][length(t[,"year"])]
    b <- t[,"year"][1]    
    qplot(t[,"year"], geom="histogram") + labs(x="Leta", y="Število nesreč") + labs(title="Število vseh nesreč po letih")

  })
  
  
  output$co2<- renderPlot({
    
    emisije <- data.frame(tbl(conn, "emissions"))
    country <- data.frame(tbl(conn, "country"))
    
    dobim_kratico <- country[country$country_name == input$emisije,"country_id"]
    nesrece <- data.frame(tbl(conn, "disaster"))
    
    pogostost <- nesrece[nesrece$country_id==dobim_kratico,"year"]
    pogostost_1960 <- pogostost[pogostost >= 1960 ]
    
    podatki_co2 <- emisije[emisije$country_id==dobim_kratico,"co2_emissions"]
    leto <- emisije[emisije$country_id==dobim_kratico,"year"]
    if (length(podatki_co2)!=0){
      par(mfrow=c(1, 2))
      plot(leto,podatki_co2,main=paste("CO2 indeks za državo",input$emisije,sep=" "),xlab="Leta",ylab="CO2 indeks")
      hist(pogostost_1960,main=paste("Stevilo nesreč za državo",input$emisije,sep=" "),xlab="Leta",ylab="Stevilo nesrec")
    }
    else{par(mfrow=c(1, 2))
         plot(c(0,0.6),c(4,4),main="",axes=FALSE,xlab="",ylab="")
         text(0.3,4,"Ni podatkov za CO2.")}
  })
  
  
  
  #Izpiše  zemljevid sveta
  output$svet<- renderPlot({
    
    t<- data.frame(arrange(filter(tbl.disaster, input$zemljevid == year), deaths))
    colnames(t)<-c("Vrsta nesrece","Drzava", "Leto", "Tip nesrece", "Pogostost", "Smrtnost", 
                   "Ranjeni", "Oskodovani", "Brez_domov", "Skoda") 
    
    tabela<-aggregate( . ~ Drzava, FUN=sum, data=t)
    
    
    sPDF <- joinCountryData2Map( tabela,
                                 joinCode = "ISO3", 
                                 nameJoinColumn = "Drzava"
                                 
    )
    mapDevice()
    mapCountryData(sPDF,
                   nameColumnToPlot=input$izberi_podatke,
                   catMethod="pretty",
                   colourPalette="terrain",
                   mapTitle=input$izberi_podatke)
  })
  
  vnesi <- function(vrstica){
    drv <- dbDriver("PostgreSQL")
    con <- dbConnect(drv, user=user , password=password, dbname=db, host=host)
    query <- build_sql("INSERT INTO disaster (country_ID,year,disaster_type_ID,occurence,deaths,injured,affected,homeless,damage) VALUES ", vrstica)
    a<-dbSendQuery(con, query)
    dbDisconnect(con)
  }
  
 # Dodajanje nove nesreče  
  id_disaster_type<-data.frame(tbl.disaster_type)
  id_country<-data.frame(tbl.country)
  observeEvent(input$vnos, {
    vrstica<<- c(       
      id_country[id_country$country_name == input$drzava, "country_id"],
      input$leto,
      id_disaster_type[id_disaster_type$disaster_type_name == input$tip_nesrece, "disaster_type_id"],
      input$pogostost,
      input$smrti,
      input$ranjeni,
      input$oskodovani,
      input$brez_doma,
      input$skoda)
    
    vnesi(vrstica)
    output$nov_vnos<- renderTable(data.frame("Država"=vrstica[1], "Leto"=vrstica[2],
                                             "Tip nesreče"=vrstica[3], "Pogostost"=vrstica[4],
                                             "Št. smrti"=vrstica[5], "Št. ranjenih"=vrstica[6],
                                             "Oškodovani"=vrstica[7], "Brez domov"=vrstica[8],
                                             "Škoda"=vrstica[9]))
    output$text1 <- renderText("Vnos uspešen.")
  })  
  
})