﻿# Naravne nesreče

Projekt pri predmetu Osnove podatkovnih baz.

## Viri podatkov

* http://www.emdat.be/advanced_search/index.html
* http://data.worldbank.org/

## Shema baze

![Shema](https://bytebucket.org/jernejzagar/naravne-nesrece/raw/549db0e1814bcfdd72a05b1717af7e250a6a0811/shema.png)

## Avtorji

* Tadej Šnajder
* Jernej Žagar
* Maša Vinter