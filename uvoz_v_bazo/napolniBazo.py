#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

import auth
import psycopg2
import csv

baza = psycopg2.connect(database=auth.db, host=auth.host, user=auth.user, password=auth.password)

def ImportData():
	'''Uvozi podatke iz csv-jev v bazo.'''
	print('Importing data ...')

	# Uvozimo podatke o nesrečah:
	with open('../podatki/podatki_nesrece.csv') as csvfile1:
			podatki_nesrece = csv.reader(csvfile1)
			next(podatki_nesrece) # izpustimo glavo csv-ja

			# Slovar indeksov za posamezen stolpec v podatki_nesrece.csv:
			inx = {
			'year' : 0,
			'disaster_type' : 2,
			'country_name' : 6,
			'occurence' : 7,
			'deaths' : 8,
			'injured' : 9,
			'affected' : 10,
			'homeless' : 11,
			'damage' : 12
			}

			cur = baza.cursor()

			# Ustvarimo tabelo 'disaster':
			# Z zanko gremo čez vse vnose v podatkih o nesrečah:
			for vrstica in podatki_nesrece:
				cur.execute("INSERT INTO disaster(country_ID,year,disaster_type_ID,occurence,deaths,injured,affected,homeless,damage) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)" ,
					(getCountry(vrstica),vrstica[inx['year']],get('disaster_type',vrstica),vrstica[inx['occurence']],vrstica[inx['deaths']],vrstica[inx['injured']],
						vrstica[inx['affected']],vrstica[inx['homeless']],vrstica[inx['damage']]))

	# Naložimo podatke na bazo:
	baza.commit()
	print('Success!')

def get(atribut,vrstica):
	'''Poišče ID atributa v trenutni vrstici, če ga ne najde, ustvari nov zapis v pripadajoči tabeli.'''

	# Slovar indeksov za posamezen stolpec v podatki_nesrece.csv:
	inx = {
	'disaster_group' : 1, 
	'disaster_type' : 2, 
	'continent' : 3, 
	'region' : 4, 
	}

	cur = baza.cursor()

	# Najde ID atributa iz trenutne vrstice v pripadajoči tabeli:
	cur.execute('SELECT ' + atribut + '_ID ' + 'FROM ' + atribut + ' WHERE ' + atribut + '_name = %s' , (vrstica[inx[atribut]],))
	vID = cur.fetchone()

	id = -1
	# Če ID ne obstaja, ga ustvari:
	if vID == None:
		if atribut == 'continent' or atribut == 'disaster_group':
			cur.execute('INSERT INTO ' + atribut + '(' + atribut + '_name' + ')' + ' VALUES (%s) ' + 'RETURNING ' + atribut + '_ID' , (vrstica[inx[atribut]],))
		elif atribut == 'region':
			cur.execute("INSERT INTO region(region_name,continent_ID) VALUES (%s,%s) RETURNING region_ID" , (vrstica[inx[atribut]],get('continent',vrstica)))
		elif atribut == 'disaster_type':
			cur.execute("INSERT INTO disaster_type(disaster_type_name,disaster_group_ID) VALUES (%s,%s) RETURNING disaster_type_ID" , (vrstica[inx[atribut]],get('disaster_group',vrstica)))

		id = cur.fetchone()[0]

	# Če obstaja:
	else:
		id = vID[0]

	cur.close()
	return id

def getCountry(vrstica):
	'''Poišče ID države v trenutni vrstici in če ga ne najde, ustvari novi zapis v tabeli 'country' in tabeli 'emissions'.'''

	# Slovar indeksov za posamezen stolpec v podatki_nesrece.csv:
	inx = {
	'country_ID' : 5, 
	'country_name' : 6, 
	}

	# Uvozimo podatke o državah:
	with open('../podatki/podatki_drzave.csv') as csvfile2:
		podatki_drzave = csv.reader(csvfile2)
		next(podatki_drzave)

		# Slovar indeksov za posamezen stolpec v podatki_drzave.csv:
		inx_d = {
		'country_name' : 2,
		'country_ID' : 3,
		'population_total' : 4,
		'surface_area' : 5,
		'population_density' : 6,
		'GNI_per_capita' : 7,
		'DRR_score' : 8
		}
		
		for drzava in podatki_drzave:
			podatki = (None,None,None,None,None)
			if drzava[inx_d['country_ID']] == vrstica[inx['country_ID']] or drzava[inx_d['country_name']] == vrstica[inx['country_name']]:
				podatki = []
				for d in drzava[4:]:
					if d == '':
						podatki.append(None)
					else:
						if d == drzava[inx_d['population_total']]:
							podatki.append(int(d))
						else:
							podatki.append(float(d))
				break

		cur = baza.cursor()
		cur.execute("SELECT country_ID FROM country WHERE country_name = %s" , (vrstica[inx['country_name']],))
		vID = cur.fetchone()

		# Če ID ne obstaja, ga ustvari:
		if vID == None:
			cur.execute("INSERT INTO country VALUES (%s,%s,%s,%s,%s,%s,%s,%s)" ,
				(vrstica[inx['country_ID']],get('region',vrstica),vrstica[inx['country_name']],podatki[0],podatki[1],podatki[2],podatki[3],podatki[4]))
			
			id = vrstica[inx['country_ID']]

			# Uvozimo podatke o co2 emisijah:
			with open('../podatki/co2_indikator.csv') as csvfile3:
				co2_indikator = csv.reader(csvfile3)
				header = next(co2_indikator)

				# Slovar indeksov za posamezen stolpec v co2_indikator.csv:
				inx_d = {
				'country_name' : 2,
				'country_ID' : 3,
				}

				for drzava in co2_indikator:
					if drzava[inx_d['country_ID']] == vrstica[inx['country_ID']] or drzava[inx_d['country_name']] == vrstica[inx['country_name']]:
						leto_indeks = 4
						for d in drzava[4:]:
							if d != '':
								cur.execute("INSERT INTO emissions VALUES (%s,%s,%s)" , (vrstica[inx['country_ID']],int(header[leto_indeks][:4]),float(d)))
								leto_indeks += 1

		# Če obstaja:
		else:			
			id = vID[0]

		cur.close()
		return id

ImportData()