#!/usr/bin/env python3.4
# -*- coding: utf-8 -*-

import auth
import psycopg2

baza = psycopg2.connect(database=auth.db, host=auth.host, user=auth.user, password=auth.password)
cur = baza.cursor()

cur.execute('DROP TABLE emissions')
cur.execute('DROP TABLE disaster')
cur.execute('DROP TABLE disaster_type')
cur.execute('DROP TABLE disaster_group')
cur.execute('DROP TABLE country')
cur.execute('DROP TABLE region')
cur.execute('DROP TABLE continent')

cur.execute('''CREATE TABLE IF NOT EXISTS continent (
  continent_ID SERIAL,
  continent_name TEXT NOT NULL,
  PRIMARY KEY (continent_ID),
  UNIQUE (continent_name)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS region (
  region_ID SERIAL,
  region_name TEXT NOT NULL,
  continent_ID SERIAL NOT NULL,
  PRIMARY KEY (region_ID),
  FOREIGN KEY (continent_ID) REFERENCES continent(continent_ID),
  UNIQUE (region_name)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS disaster_group (
  disaster_group_ID SERIAL,
  disaster_group_name TEXT NOT NULL,
  PRIMARY KEY (disaster_group_ID),
  UNIQUE (disaster_group_name)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS disaster_type (
  disaster_type_ID SERIAL,
  disaster_type_name TEXT NOT NULL,
  disaster_group_ID SERIAL NOT NULL,
  PRIMARY KEY (disaster_type_ID),
  FOREIGN KEY (disaster_group_ID) REFERENCES disaster_group(disaster_group_ID),
  UNIQUE (disaster_type_name)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS country (
  country_ID CHAR(3),
  region_ID SERIAL NOT NULL,
  country_name TEXT NOT NULL,
  population_total BIGINT,
  surface_area REAL,
  population_density REAL,
  GNI_per_capita REAL,
  DRR_score REAL,
  PRIMARY KEY (country_ID),
  FOREIGN KEY (region_ID) REFERENCES region(region_ID),
  UNIQUE (country_name)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS emissions (
  country_ID CHAR(3),
  year SMALLINT,
  co2_emissions REAL,
  PRIMARY KEY (country_id, year),
  FOREIGN KEY (country_ID) REFERENCES country(country_ID)
  )''')

cur.execute('''CREATE TABLE IF NOT EXISTS disaster (
  disaster_ID SERIAL,
  country_ID CHAR(3) NOT NULL,
  year SMALLINT NOT NULL,
  disaster_type_ID SERIAL,
  occurence INTEGER,
  deaths INTEGER,
  injured INTEGER,
  affected INTEGER,
  homeless INTEGER,
  damage REAL,
  PRIMARY KEY (disaster_ID),
  FOREIGN KEY (country_ID) REFERENCES country(country_ID),
  FOREIGN KEY (disaster_type_ID) REFERENCES disaster_type(disaster_type_ID)
  )''')

baza.commit()
